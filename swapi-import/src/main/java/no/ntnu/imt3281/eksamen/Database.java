package no.ntnu.imt3281.eksamen;

import org.json.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    private Connection con;

    Database(){
        String dbURL = "jdbc:derby:../swapiDB";
        con = null;
        try {                                                   // Attempt to connect to DB
            con = DriverManager.getConnection(dbURL);
        } catch (SQLException e) {                              // if it failed
            if (e.getMessage().equals("Database '../swapiDB' not found.")) {    // Is the Database missing
                try {                                           // Attempt to create the DB

                    con = DriverManager.getConnection(dbURL+";create=true");
                    //String absolutepath = "C:/Users/vegard/Desktop/EksamenApp2019/imt3281-h2019-kontinuasjon/mydatabase.txt";
                    String sql = new String(Files.readAllBytes(Paths.get("mydatabase.txt")));

                    Statement stmnt = con.createStatement();
                    stmnt.execute(sql);
                    System.out.println("Table PLANETS created");
                } catch (SQLException e1) {                     // If creation failed, exit
                    System.err.println("Kunne ikke opprette databasen");
                    System.exit(1);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } else {                                            // Database exists, but could not connect. Exit.
                System.err.println("Kunne ikke koble til databasen");
                System.exit(1);
            }
        }

        URL url = null;
        try {
            url = new URL("https://swapi.co/api/films/1/");           // Get information about film #1
            URLConnection connection = url.openConnection();                // Need a bit extra
            // Must set user-agent, the java default user agent is denied
            connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
            // Must set accept to application/json, if not html is returned
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String tmp;
            while ((tmp = br.readLine())!=null) {                           // Dump contents to console
                System.out.println(tmp);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addSpecieToDB(JSONObject json) {
        String sql = String.format("INSERT INTO SPECIES(NAME, CLASSIFICATION, DESIGNATION, AVRAGE_HEIGHT, SKIN_COLORS, " +
                        " HAIR_COLOr, EYE_COLORS, AVERAGE_LIFESPAN, HOMEWORLD, LANGUAGE, PEOPLE, FILMS, CREATED, EDITED, URL)" +
                        " VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", json.get("name"), json.get("classification"), json.get("designation"),
                json.get("average_height"), json.get("skin_color"), json.get("hair_color"), json.get("eye_color"), json.get("average_lifespan"), json.get("homeworld"), json.get("language"), json.get("people"),
                json.get("films"), json.get("created"), json.get("edited"), json.get("url"));
        try {
            Statement stmnt = con.createStatement();
            int rows = stmnt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addStarshipToDB(JSONObject json) {
        String sql = String.format("INSERT INTO STARSHIPS(NAME, MODEL, MANUFACTURER, COST_IN_CREDITS, " +
                        "LENGHT, MAX_ATMOSPHERING_SPEED, CREW, PASSANGERS, CARGO_CAPACITY, CONSUMABLES, HYPERDRIVE_RATING," +
                        "MGLT, STARSHIP_CLASS, PILOTS, FILMS, CREATED, EDITED, URL) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, " +
                        "%s, %s, %s, %s, %s, %s, %s, %s, %s)", json.get("name"), json.get("model"), json.get("manufacturer"), json.get("cost_in_credits"), json.get("length"),
                json.get("max_atmosphering_speed"), json.get("crew"), json.get("passengers"), json.get("cargo_capacity"), json.get("cosumables"), json.get("hyperdrive_rating"), json.get("MGLT"),
                json.get("starship_class"), json.get("pilots"), json.get("films"), json.get("created"), json.get("edited"), json.get("url"));
        try {
            Statement stmnt = con.createStatement();
            int rows = stmnt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addVehicleToDB(JSONObject json) {
        String sql = String.format("INSERT INTO VEHICLE(NAME, MODEL, MANUFACTURER, COST_IN_CREDITS, LENGTH, MAX_ATMOSPHERING_SPEED" +
                        " , CREW, PASSANGERS, CARGO_CAPACITY, CONSUMABLES, VEHICLE_CLASS, PILOTS, FILMS, CREATED, EDITED, URL) " +
                        " VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", json.get("name"), json.get("model"), json.get("manufacturer"),
                json.get("cost_in_credits"), json.get("length"), json.get("max_atmosphering_speed"), json.get("crew"), json.get("passengers"), json.get("cargo_capacity"), json.get("consumables"), json.get("vehicle_class"),
                json.get("pilots"), json.get("films"), json.get("created"), json.get("edited"), json.get("url"));
        try {
            Statement stmnt = con.createStatement();
            int rows = stmnt.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPlanetToDB(JSONObject json){
        String sql = String.format("INSERT INTO PLANETS (NAME, ROTATION_PERIODE, ORBITAL_PERIODE, DIAMETER, " +
                        " CLIMATE, GRAVITY, TERRAIN, SURFACE_WATER, POPULATION, RESIDENTS, FILMS, CREATED, EDITED, URL)" +
                        " VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", json.get("name"), json.get("rotation_period"),
                json.get("orbital_period"), json.get("diameter"), json.get("climate"), json.get("gravity"), json.get("terrain"), json.get("surface_water"), json.get("population"), json.get("residents"),
                json.get("films"), json.get("created"), json.get("edited"), json.get("url"));
        try {
            Statement stmnt = con.createStatement();
            int rows = stmnt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFilmToDB(JSONObject json){
        String sql = String.format("INSERT INTO FILMS (TITLE, EPISODE_ID, OPENING_CRAWL, DIRECTOR, PRODUCER, REALEASE_DATE" +
                        " CHARACTERS, PLANETS, STARSHIPS, VEHICLES, SPECIES, CREATED, EDITED, URL) VALUES(%s, %s, %s, %s, %s, " +
                        "%s, %s, %s, %s, %s, %s, %s, %s, %s)", json.get("title"), json.get("episode"), json.get("opening_crawl"), json.get("director"), json.get("producer"), json.get("release_date"),
                json.get("characters"), json.get("planets"), json.get("starshpis"), json.get("vehicles"), json.get("species"), json.get("created"), json.get("edited"), json.get("url"));

        try {
            Statement stmnt = con.createStatement();
            int rows = stmnt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPeopleToDB(JSONObject json){
        String sql = String.format("INSERT INTO PEOPLE (NAME, HEIGHT, MASS, HAIR_COLOR, SKIN_COLOR, EYE_COLOR, BIRTH_YEAR" +
                        " GENDER, HOMEWORLD, FILMS, SPECIES, VEHICLES, STARSHIPS, CREATED, EDITED, URL) VALUES(%s, %s, %s, %s, %s, " +
                        "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", json.get("name"), json.get("height"), json.get("mass"), json.get("hair_color"), json.get("skin_color"), json.get("eye_color"),
                json.get("birth_year"), json.get("gender"), json.get("homeworld"), json.get("films"), json.get("species"), json.get("vehicles"), json.get("starships"), json.get("created"), json.get("edited"), json.get("url"));
        Statement stmnt = null;
        try {
            stmnt = con.createStatement();
            int rows = stmnt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
