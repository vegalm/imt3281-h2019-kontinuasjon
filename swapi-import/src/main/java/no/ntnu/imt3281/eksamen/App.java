package no.ntnu.imt3281.eksamen;

import java.util.concurrent.*;

/**
 * Utgangspunkt for swapi-import oppgaven
 */
public class App {
    private static LinkedBlockingQueue<String> visited;
    private static LinkedBlockingQueue<String> notVisited;
    private static Database db;
    private static final int PRODUCERS = 5;

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newCachedThreadPool();

        db = new Database();
        visited = new LinkedBlockingQueue<>();
        notVisited = new LinkedBlockingQueue<>();

        for(int i = 0; i < PRODUCERS; i++){
            executor.execute(new Producer(visited, notVisited, db));
        }

        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);
    }
}