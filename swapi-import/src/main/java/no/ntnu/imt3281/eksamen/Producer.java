package no.ntnu.imt3281.eksamen;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;

import org.json.JSONArray;
import org.json.JSONObject;

public class Producer implements Runnable {
    private  LinkedBlockingQueue<String> visited;
    private  LinkedBlockingQueue<String> notVisited;
    private  Database db;
    private static final int MAX_ENTRIES = 268;

    Producer(LinkedBlockingQueue visited, LinkedBlockingQueue notVisited, Database db) {
        this.visited = visited;
        this.notVisited = notVisited;
        this.db = db;
    }

    @Override
    public void run(){
        Boolean run = true;

        while(run){
            if(notVisited.size()>=1){
                String url = getUnvisitedUrl();
                if(!url.isEmpty()){
                    JSONObject json = readJsonFromUrl(url);
                    addToDB(json);
                    checkJsonForLinks(json);
                }
            }
            else{
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            run = continueLoop();
        }
    }

    private void checkJsonForLinks(JSONObject json){
       Iterator<String> keys = json.keys();

       while(keys.hasNext()){
           String key = keys.next();
           if(json.get(key) instanceof JSONArray || json.get(key) instanceof String){
              JSONArray tmp = (JSONArray) json.get(key);
              int length = tmp.length();
              for(int i = 0; i<=length; i++){
                  String value = (String)tmp.get(i);
                  if(value.contains("https://swapi.co/api")){
                      addLinksToNotVisited(value);
                  }
              }
           }
       }
    }

    private synchronized void addLinksToNotVisited(String s){
        if(!notVisited.contains(s)) {
            try {
                notVisited.put(s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void addToDB(JSONObject json){
        String id = (String)json.get("url");
        if(id.contains("planets")){
            db.addPlanetToDB(json);
        }
        else if(id.contains("films")){
            db.addFilmToDB(json);
        }
        else if(id.contains("people")){
            db.addPeopleToDB(json);
        }
        else if(id.contains("species")){
            db.addSpecieToDB(json);
        }
        else if(id.contains("starships")){
            db.addStarshipToDB(json);
        }
        else if(id.contains("vehicles")){
            db.addVehicleToDB(json);
        }
    }

    private synchronized String getUnvisitedUrl() {
        Iterator iterator = notVisited.iterator();
        String url = "";
        boolean found = false;
        while (iterator.hasNext() && !found) {
            url = (String) iterator.next();
            if(!visited.contains(url)){
                found = true;
                visited.add(url);
            }
            else iterator.remove();
        }
        return url;
    }

    private Boolean continueLoop(){
        return (visited.size()>= MAX_ENTRIES);
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String s) {
        JSONObject json = null;
        try {
            URL url = new URL(s);
            URLConnection connection = url.openConnection();                // Need a bit extra
            // Must set user-agent, the java default user agent is denied
            connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
            // Must set accept to application/json, if not html is returned
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String jsonText = readAll(br);
            json = new JSONObject(jsonText);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }
}
